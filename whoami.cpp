#include "whoami.h"
#include <cstdlib>
#include <iomanip>
#include <pwd.h>
#include <sstream>
#include <unistd.h>

const std::array<std::array<std::string_view, 2>, 2> uid::WhoAmI::OPT_DESCS{
    {{"--help\t", "display this help and exit"},
     {"--version", "output version information and exit"}}};

const std::string uid::WhoAmI::
    PROGRAM_NAME = "whoami",
  uid::WhoAmI::USAGE_INFO =
      "Usage: " + PROGRAM_NAME +
      " [OPTION]...\nPrint the user name associated with the current effective "
      "user ID.\nSame as id -un.\n\n",
  uid::WhoAmI::ANCILLARY_INFO =
      std::string(
          "GNU coreutils online help: "
          "<https://www.gnu.org/software/coreutils/>\n"
          "Full documentation <https://www.gnu.org/software/coreutils/") +
      PROGRAM_NAME + ">\nor available locally via: info '(coreutils) " +
      PROGRAM_NAME + " invocation'\n",
  uid::WhoAmI::VER_INFO =
      PROGRAM_NAME +
      " (GNU coreutils) 9.4\n"
      "Copyright (C) 2023 Free Software Foundation, Inc.\n"
      "License GPLv3+: GNU GPL version 3 or later "
      "<https://gnu.org/licenses/gpl.html>.\n"
      "This is free software: you are free to change and redistribute it.\n"
      "There is NO WARRANTY, to the extent permitted by law.\n\n"
      "Written by Richard Mlynarik.";
const uid_t uid::WhoAmI::NO_UID = -1;

void uid::WhoAmI::ver() { std::cout << VER_INFO << std::endl; }
void uid::WhoAmI::help() {
  std::ostringstream osstream(USAGE_INFO, std::ios::ate);

  for (const std::array<std::string_view, 2> &OPT_DESC : OPT_DESCS) {
    for (const std::string_view &STR : OPT_DESC)
      osstream << "\t" << STR << "\t";
    osstream << "\n";
  }
  osstream << "\n" << ANCILLARY_INFO << std::endl;
  std::cout << osstream.str();
}

void uid::WhoAmI::main() {
  const uid_t UID = geteuid();
  const struct passwd *const pw =
      UID == uid::WhoAmI::NO_UID && errno ? nullptr : getpwuid(UID);

  if (!pw) {
    std::cerr << "Error cannot find name for user ID " << UID << std::endl;
    exit(EXIT_FAILURE);
  }

  std::cout << pw->pw_name << std::endl;
}
