#include <array>
#include <iostream>
#include <pwd.h>
#include <string_view>

namespace uid {
class WhoAmI {
public:
  static const std::string PROGRAM_NAME, ANCILLARY_INFO, USAGE_INFO, VER_INFO;
  static const std::array<std::array<std::string_view, 2>, 2> OPT_DESCS;
  static const uid_t NO_UID;

  static void help();
  static void ver();
  static void main();
};
} // namespace uid
