#include "whoami.h"
#include <cstring>
#include <getopt.h>
#include <iostream>

static void parse(const int argc, char *const *const argv, bool &isRun) {
  static struct option long_options[] = {{"help", no_argument, 0, 'h'},
                                         {"version", no_argument, 0, 'v'},
                                         {0, 0, 0, 0}};
  int c;
  while ((c = getopt_long(argc, argv, "h:v:", long_options, 0)) != -1) {
    switch (c) {
    case 'h':
      uid::WhoAmI::help();
      isRun = false;
      break;
    case 'v':
      uid::WhoAmI::ver();
      isRun = false;
      break;
    case '?':
      break;
    default:
      break;
    }
  }
}

int main(const int argc, char *const *const argv) {
  bool isRun = true;
  parse(argc, argv, isRun);

  if (isRun)
    uid::WhoAmI::main();
}
