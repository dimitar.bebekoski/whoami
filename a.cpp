#include "whoami.h"
#include <array>
#include <cstring>
#include <iostream>
#include <pwd.h>
#include <sstream>
#include <string_view>
#include <unistd.h>

int main(int argc, char **argv) {
  if (argc > 1 && !strcmp(argv[1], "--help")) {
    uid::WhoAmI::usage();
    return 0;
  }

  uid_t uid = geteuid();
  struct passwd *pw =
      uid == uid::WhoAmI::NO_UID && errno ? 0 : getpwuid(geteuid());
  if (!pw) {
    printf("XD\n");
    return 1;
  }

  std::cout << pw->pw_name;
  return 0;
}
